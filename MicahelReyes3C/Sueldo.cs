﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicahelReyes3C
{
    class Sueldo
    {
        private int horasTrabajo;
        private int mesesTrabajo;
        private int años;
        private int mesesAnuales =12;
        private int sueldoMensual = 500;
        private int sueldoHora = 2 ;
        int Total;

        public int HorasTrabajo
        {
            get { return horasTrabajo; }
            set { horasTrabajo = value; }
        }

        public int MesesTrabajo
        {
            get { return mesesTrabajo; }
            set { mesesTrabajo = value; }
        }

        public int Años
        {
            get { return años; }
            set { años = value; }
        }
        public int MesesAnuales
        {
            get { return mesesAnuales; }
            set { mesesAnuales = value; }
        }

        public int SueldoMensual
        {
            get { return sueldoMensual; }
            set { sueldoMensual = value; }
        }

        public int SueldoHoras
        {
            get { return sueldoHora; }
            set { sueldoHora = value; }
        }

        public void PagoTemporal()
        {
            Total = sueldoMensual * mesesTrabajo;
            Console.WriteLine("Valor Total de Dinero por meses trabajados: " + Total);
        }
        public void PagoFijo()
        {
            Total = (sueldoMensual* mesesAnuales)*años;
            Console.WriteLine("Valor Total de Dinero por Años trabajados: " + Total);
        }
        public void PagoHoras()
        {
            Total = sueldoHora * horasTrabajo;
            Console.WriteLine("Valor Total de Dinero de Horas Trabajadas: " + Total);
        }

    }
}
