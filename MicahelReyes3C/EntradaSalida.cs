﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicahelReyes3C
{
    class EntradaSalida
    {
        private String entrada;
        private String salida;


        public String Entrada
        {
            get { return entrada; }
            set { entrada = value; }
        }

        public String Salida
        {
            get { return salida; }
            set { salida = value; }
        }


    }
}
