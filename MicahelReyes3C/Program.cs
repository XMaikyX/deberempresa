﻿using System;

namespace MicahelReyes3C
{
    class Program
    {
        static void Main(string[] args)
        {
            // EMPLEADO TEMPORAL
            Console.WriteLine(" PAGO DE EMPLEADO TEMPORAL");
            Empleado empleado = new Empleado();
            empleado.nombre = "Michael Alexander";
            empleado.edad = "20";
            empleado.departamento = "Servicios de Compra";

            //valores de trabajo
            Sueldo sueldo=new Sueldo();
            sueldo.MesesTrabajo = 5;
            EntradaSalida entrada = new EntradaSalida();
            entrada.Entrada = "24 de Abril del 2016";
            entrada.Salida = "28 de Agosto del 2016";
           
            Console.WriteLine("Nombre del : \n "+empleado.nombre);
            Console.WriteLine("Edad del Empleado: \n " + empleado.edad);
            Console.WriteLine("Departamento :\n" + empleado.departamento);
            Console.WriteLine("Sueldo Mensual: " + sueldo.SueldoMensual);
            Console.WriteLine("Fecha de Entrada: " + entrada.Entrada + "\nFecha de Salida: " + entrada.Salida
                + "\nMeses totales de Trabajo: " + sueldo.MesesTrabajo);

            // forma de pago
            sueldo.PagoTemporal();

            //EMPLEADO FIJO
            Console.WriteLine("\n \n PAGO DE EMPLEADO FIJO");
            Empleado empleado1 = new Empleado();
            empleado1.nombre = "Jean Pierre";
            empleado1.edad = "39";
            empleado1.departamento = "Servicios de Tecnico";

            //valores de trabajo
            Sueldo sueldo1 = new Sueldo();
            sueldo1.MesesAnuales= sueldo1.MesesAnuales;
            sueldo.Años = 3;
            Console.WriteLine("Nombre del : \n " + empleado1.nombre);
            Console.WriteLine("Edad del Empleado: \n " + empleado1.edad);
            Console.WriteLine("Departamento :\n" + empleado1.departamento);
            Console.WriteLine("Años de Trabajo: " + sueldo1.Años);
            Console.WriteLine("Sueldo Mensual: "+sueldo1.SueldoMensual);
            sueldo.PagoFijo();

            //EMPLEADO POR HORA
            Console.WriteLine("\n \n PAGO DE EMPLEADO POR HORA");
            Empleado empleado2 = new Empleado();
            empleado2.nombre = "Jeremy Josue";
            empleado2.edad = "25";
            empleado2.departamento = "Servicios de Tecnico";

            //valores de trabajo
            Sueldo sueldo2 = new Sueldo();
            sueldo.HorasTrabajo = 8;
            sueldo.SueldoHoras = sueldo.SueldoHoras;
            Console.WriteLine("Nombre del : \n " + empleado1.nombre);
            Console.WriteLine("Edad del Empleado: \n " + empleado1.edad);
            Console.WriteLine("Departamento :\n" + empleado1.departamento);
            Console.WriteLine("Horas Traajadas: " + sueldo2.HorasTrabajo);
            Console.WriteLine("Pago por Horas: " + sueldo.SueldoHoras);
            sueldo.PagoHoras();
        }
    }
}
